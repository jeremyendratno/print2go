import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var btnOrder: UIButton!
    @IBOutlet weak var btnSmall1: UIButton!
    @IBOutlet weak var btnSmall2: UIButton!
    @IBOutlet weak var btnInfo: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ShadowLayer
        btnOrder.layer.shadowColor = UIColor.black.cgColor
        btnOrder.layer.shadowOpacity = 0.5
        btnOrder.layer.shadowOffset = .zero
        btnOrder.layer.shadowRadius = 10
        btnSmall1.layer.shadowColor = UIColor.black.cgColor
        btnSmall1.layer.shadowOpacity = 0.2
        btnSmall1.layer.shadowOffset = .zero
        btnSmall1.layer.shadowRadius = 5
        btnSmall2.layer.shadowColor = UIColor.black.cgColor
        btnSmall2.layer.shadowOpacity = 0.2
        btnSmall2.layer.shadowOffset = .zero
        btnSmall2.layer.shadowRadius = 5
        
        
    }
    //ButtonStuff
    @IBAction func btnInfo(_ sender: Any) {
        let alert = UIAlertController(title: "Information", message: "You can start printing your own documents from home without having to go out and queue, start by ordering now !", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    @IBAction func btnOrder(_ sender: Any) {
        
    }
    @IBAction func btnSmall1(_ sender: Any) {
        
    }
    @IBAction func btnSmall2(_ sender: Any) {
        
    }
    
}
