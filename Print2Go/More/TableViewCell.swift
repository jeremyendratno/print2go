import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var Label1: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setCell(Settings : String){
        Label1.text = Settings
    }
}
