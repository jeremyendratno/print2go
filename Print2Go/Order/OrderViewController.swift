import UIKit
import MobileCoreServices

class OrderViewController: UIViewController, UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
              guard let myURL = urls.first else {
                   return
              }
        dataOrder.append(myURL)
        print("import URL : \(dataOrder.popLast())")
    }
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
            print("view was cancelled")
            dismiss(animated: true, completion: nil)
    }
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var labelnama: UILabel!
    @IBOutlet weak var labelalamat: UILabel!
    @IBOutlet weak var pemberitahuan: UILabel!
    @IBOutlet weak var coppies: UILabel!
    
    
    var datatempat : [InitDataTempat] = []
    var dataOrder: [URL] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        datatempat = DataTempat().datatempat()
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func uploadImage(){
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
        
    }
    
    @IBAction func btnUpload(_ sender: Any) {
        uploadImage()
    }
    
    @IBAction func coppiesstepper(_ sender: UIStepper) {
        coppies.text = Int(sender.value).description + " Coppies"
    }
    
}

extension OrderViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        datatempat.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderCell", for: indexPath) as! CollectionViewCell
        let cdata = datatempat[indexPath.row]
        cell.set(data: cdata)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        for i in 0...datatempat.count {
            let otherindex : IndexPath = [0, i]
            let othercell = collectionView.cellForItem(at: otherindex)
            othercell?.layer.borderColor = UIColor.clear.cgColor
        }
        
        pemberitahuan.isHidden = true
        
        labelnama.text = datatempat[indexPath[1]].namatempat
        labelalamat.text = "Address - " + datatempat[indexPath[1]].alamat
        
        let cell = collectionView.cellForItem(at: indexPath)
        cell?.layer.borderColor = UIColor.link.cgColor
        cell?.layer.borderWidth = 3
        cell?.layer.masksToBounds = false
        cell?.layer.cornerRadius = 62.5
        cell?.clipsToBounds = true
    }
    
    
}




