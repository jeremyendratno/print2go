import UIKit



class CollectionViewCell: UICollectionViewCell {
    

    @IBOutlet weak var image1: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        image1.makeRounded()
    }
    
    func set(data : InitDataTempat) {
        image1.image = data.image1
    }
}

extension UIImageView {

    func makeRounded() {
        
        self.layer.borderColor = UIColor.secondaryLabel.cgColor
        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
    
    func makeShadow(){
        
        self.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 0.5
        self.clipsToBounds = false
    }
    
    
}
