import Foundation
import UIKit

class InitDataTempat {
    var image1 : UIImage
    var namatempat : String
    var alamat : String
    
    init(image1 : UIImage, namatempat : String, alamat : String) {
        self.image1 = image1
        self.namatempat = namatempat
        self.alamat = alamat
    }
}

class DataTempat {
    func datatempat() ->[InitDataTempat] {
        var datatempattemp : [InitDataTempat] = []
        
        let data1 = InitDataTempat(image1: UIImage(named: "Print2Go_universitasciputra")!, namatempat: "Universitas Ciputra", alamat: "CitraLand CBD Boulevard, Made, Kec. Sambikerep, Kota SBY, Jawa Timur 60219")
        datatempattemp.append(data1)
        
        let data2 = InitDataTempat(image1: UIImage(named: "Print2Go_kozko")!, namatempat: "Kozko Printing", alamat: "Kozko BC02 made, Jl. Citraland Surabaya, Made, Kec. Sambikerep, Kota SBY, Jawa Timur 60229")
        datatempattemp.append(data2)
        
        let data3 = InitDataTempat(image1: UIImage(named: "Print2Go_ptc")!, namatempat: "Pakuwon Trade Centre", alamat: "Jl. Mayjen Yono Suwoyo No.2, Babatan, Kec. Wiyung, Kota SBY, Jawa Timur 60216")
        datatempattemp.append(data3)
        
        let data4 = InitDataTempat(image1: UIImage(named: "Print2Go_tpr")!, namatempat: "Citraland Fresh Market", alamat: "Jl. Taman Puspa Raya, Made, Kec. Sambikerep, Kota SBY, Jawa Timur 60217")
        datatempattemp.append(data4)
        
        return datatempattemp
    }
}




