import UIKit

class HistoryViewController: UIViewController {
    var dbnum: [Int] = [100001, 100002, 100003]
    var dbinfo: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
             
    }
    @IBAction func btnInfo(_ sender: Any) {
        let alert = UIAlertController(title: "Information", message: "You can start printing your own documents from home without having to go out and queue, start by ordering now !", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
}
extension HistoryViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dbnum.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tbv = tableView.dequeueReusableCell(withIdentifier: "tbvc", for: indexPath) as! TableViewCellHistory
        tbv.txtCell.text = "Order No. \(dbnum[indexPath.row])"
        return tbv
    }
    
    
}
