//
//  TableViewCellHistory.swift
//  Print2Go
//
//  Created by Jovan Alvin on 05/05/20.
//  Copyright © 2020 AnakAnakLonte. All rights reserved.
//

import UIKit

class TableViewCellHistory: UITableViewCell {
    @IBOutlet weak var txtCell: UILabel!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRIght: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnLeft.layer.shadowColor = UIColor.black.cgColor
        btnLeft.layer.shadowOpacity = 0.2
        btnLeft.layer.shadowOffset = .zero
        btnLeft.layer.shadowRadius = 5
        btnRIght.layer.shadowColor = UIColor.black.cgColor
        btnRIght.layer.shadowOpacity = 0.2
        btnRIght.layer.shadowOffset = .zero
        btnRIght.layer.shadowRadius = 5

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
